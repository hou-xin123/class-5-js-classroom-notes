## Promise

在JavaScript的世界中，所有代码都是单线程执行的。

由于这个“缺陷”，导致JavaScript的所有网络操作，浏览器事件，都必须是异步执行。异步执行可以用回调函数实现：
```
function callback() {
    console.log('Done');
}
console.log('before setTimeout()');
setTimeout(callback, 1000); // 1秒钟后调用callback函数
console.log('after setTimeout()');
```
可见，异步操作会在将来的某个时间点触发一个函数调用。

AJAX就是典型的异步操作。
eg：
```
request.onreadystatechange=function(){
    if (request.readyState===4){
        if(request.status===200){
            return success (request.responseText);
        } else {
            return fail(request.status);
        }
    }
}
```
把回调函数success(request.responseText)和fail(request.status)写到一个AJAX操作里很正常，但是不好看，而且不利于代码复用。

Promise有各种开源实现，在ES6中被统一规范，由浏览器直接支持。先测试一下你的浏览器是否支持Promise：
```
'use strict';

new Promise(function () {});
// 直接运行测试:
console.log('支持Promise!');
```

eg:生成一个0-2之间的随机数，如果小于1，则等待一段时间后返回成功，否则返回失败：
```
function test (resolve,reject){
    var timeOut=Math.random()*2;
    log('set timeout to:'+timeOut+'seconds.');
    setTimeout(function(){
        if(timeOut<1){
            log('call resolve()...');
            resolve('200 OK');
        }
        else{
            log('call reject()...');
            reject('timeout in'+timeOut+'seconds.');
        }
    },timeOut*1000);
}
```
我们就可以用一个Promise对象来执行它，并在将来某个时刻获得成功或失败的结果：
```
var p1 =new Promise(test);
var p2 =p1.then(function(result){
    console.log('成功：'+result);
});
var p3 =p2.catch(function(reason)
{
    console.log('失败：'+reason);
});
```

