## itrtable
遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。
### 具有iterable类型的集合可以通过新的for ... of循环来遍历。
```
用for...of循环遍历集合，用法如下：
var a =['A','B','C'];
var s =new Set(['A','B','C']);
var m =new Map([1,'x'],[2,'y'],[3,'z']);
for(var x of a ){
//遍历Array
console.log(x);
}
for(var x of s ){
//遍历Set
console.log(x);
}
for(var x of m ){
//遍历Map
console.log(x[0]+'='+x[1]);
}
```
更好的方式是直接使用iterable内置的forEach方法，它接收一个函数，每次迭代就自动回调该函数。以Array为例：
```
'use strict';
var a=['A','B','C'];
a.forEach(function(element,index,array){
    // element: 指向当前元素的值
    // index: 指向当前索引
    // array: 指向Array对象本身
    console.log(element+',index='+index);
});

```
### Set与Array类似，但Set没有索引，因此回调函数的前两个参数都是元素本身：
```
var s = new Set(['A', 'B', 'C']);
s.forEach(function (element, sameElement, set) {
    console.log(element);
});
```

### Map的回调函数参数依次为value、key和map本身：
```
var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
m.forEach(function (value, key, map) {
    console.log(value);
});
```

## 函数

### 抽象
抽象是数学中非常常见的概念。

## 函数定义和调用

### 定义函数
上述abs（）函数的定义如下：
```
1.function指出这是一个函数定义；
2.abs是函数的名称；
3.（x）括号内列出函数的参数，多个参数以，分隔；
4.{...}之间的代码是函数体，可以包含若干语句，甚至可以没有任何语句。
```


var abs = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};

```
在这种方式下，function (x) { ... }是一个匿名函数，它没有函数名。但是，这个匿名函数赋值给了变量abs，所以，通过变量abs就可以调用该函数。

上述两种定义完全等价，注意第二种方式按照完整语法需要在函数体末尾加一个;，表示赋值语句结束。
```