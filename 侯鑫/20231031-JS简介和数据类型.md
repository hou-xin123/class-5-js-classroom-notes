## JavaScript 简介
### 为什么我们要学JavaScript？
在Web世界里，只有JavaScript能跨平台、跨浏览器驱动网页，与用户交互。
JavaScript一度被认为是一种玩具编程语言，它有很多缺陷，所以不被大多数后端开发人员所重视。很多人认为，写JavaScript代码很简单，并且JavaScript只是为了在网页上添加一点交互和动画效果。

## JavaScript 基本语法
JavaScript的语法和Java语言类似，每个语句以;结束，语句块用{...}。但是，JavaScript并不强制要求在每个语句的结尾加;，浏览器中负责执行JavaScript代码的引擎会自动在每个语句的结尾补上;。

一、js引入

```
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>变量</title>
    </head>
    <body>
        <h1>JavaScript 变量</h1>
        <script type="text/JavaScript">
            var name='zhangsan';
            //alert(name);
            //console.log(name);
            document.write(name);
        </script>
    </body>
```
## 数据类型
1.Number：该类型的表示方法有两种形式，第一种是整数，第二种为浮点数。整数：可以通过十进制，八进制，十六进制的字面值来表示。浮点数：就是该数值中必须包含一个小数点，且小数点后必须有一位数字。

2.字符串
字符串是以单引号'或双引号"括起来的任意文本，比如'abc'，"xyz"等等。请注意，''或""本身只是一种表示方式，不是字符串的一部分，因此，字符串'abc'只有a，b，c这3个字符。

3.布尔值
布尔值和布尔代数的表示完全一致，一个布尔值只有true、false两种值，要么是true，要么是false，可以直接用true、false表示布尔值，也可以通过布尔运算计算出来：
&&运算是与运算，只有所有都为true，&&运算结果才是true：
||运算是或运算，只要其中有一个为true，||运算结果就是true：
!运算是非运算，它是一个单目运算符，把true变成false，false变成true：

4.比较运算符
当我们对Number做比较时，可以通过比较运算符得到一个布尔值：
要特别注意相等运算符==。JavaScript在设计时，有两种比较运算符：
第一种是==比较，它会自动转换数据类型再比较，很多时候，会得到非常诡异的结果；
第二种是===比较，它不会自动转换数据类型，如果数据类型不一致，返回false，如果一致，比较。
由于JavaScript这个设计缺陷，不要使用==比较，始终坚持使用===比较。

5.null和undefined
null表示一个“空”的值，它和0以及空字符串''不同，0是一个数值，''表示长度为0的字符串，而null表示“空”。
在其他语言中，也有类似JavaScript的null的表示，例如Java也用null，Swift用nil，Python用None表示。但是，在JavaScript中，还有一个和null类似的undefined，它表示“未定义”。
JavaScript的设计者希望用null表示一个空的值，而undefined表示值未定义。事实证明，这并没有什么卵用，区分两者的意义不大。大多数情况下，我们都应该用null。undefined仅仅在判断函数参数是否传递的情况下有用。

6.数组
数组是一组按顺序排列的集合，集合的每个值称为元素。JavaScript的数组可以包括任意数据类型