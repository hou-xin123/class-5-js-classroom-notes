## 闭包

### 函数作为返回值
高阶函数除了可以接受函数作为参数外，还可以把函数作为结果值返回。
```
function sum(arr){
    var sum =function(){
        return arr.reduce(function(x,y){
            return x+y;
        })
    }
    
    return sum;
}
```
闭包，就是一个函数（被一个函数返回的函数），并不是所有被返回的函数都称之为闭包。除了被返回，这个函数应该还必须引用了外部函数中传入的参数或者定义的变量，那么这样的一个返回函数，才称之为闭包

### 闭包
```
function count(){
    var arr =[];
    for (var i=1; i<=3;i++){
        arr.push((function(n){
            return function(){
                return n*n;
            }
        })(i));
    }
    return arr;
}
var results =count();
var f1 =results[0];
var f2 =results[1];
var f3 =results[2];
f1();
f2();
f3();
```